<?php

namespace Impacte\Octadesk;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery as m;
use Phpunit\Framework\TestCase;

class ResponseParserTest extends TestCase
{
    public function setUp(): void
    {
        $this->setUpMocks();
        $this->sut = new ResponseParser($this->httpClient);        
    }

    protected function setUpMocks()
    {
        $this->httpClient   = m::mock(Client::class)->makePartial();
        $this->httpResponse = m::mock(Response::class)->makePartial();
    }

    /** @test */
    public function should_parse_httpclient_json_response()
    {
        $this->httpClient->shouldReceive('get')
                   ->once()
                   ->andReturn($this->httpResponse);
        
        $this->httpResponse->shouldReceive("getBody")
                     ->once()
                     ->andReturnSelf();

        $this->httpResponse->shouldReceive("getContents")
                     ->once()
                     ->andReturn('{"json":"response"}');

        $response = $this->sut->get('someurl');

        $this->assertInstanceOf(\stdClass::class, $response);
        $this->assertEquals("response", $response->json);
    }

    public function should_have_methods_whitelist_http_client()
    {
        $this->assertTrue();
        //$this->assertTrue($this->sut->checkMethodName('post'));
        //$this->assertTrue($this->sut->checkMethodName('put'));
    }

    public function should_throw_exception_when_calling_non_whitelist_method()
    {
        $result = $this->sut->petris();
        die($result);
    }

}
