<?php

namespace Impacte\Octadesk\Endpoints;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Psr7\Response;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\FakeHttpResponse;

class TicketsTest extends TestCase
{
    use FakeHttpResponse;

    protected $sut;

    protected $httpClient;

    public function setUp(): void
    {
        $this->httpClient = m::mock(HttpClient::class);
        $this->sut        = new Tickets($this->httpClient);
    }

    /** @test */
    public function should_create_new_ticket()
    {
        $httpResponse = $this->createFakeHttpResponse();
        $data         = ['sampledata' => 'data'];
        
        $this->httpClient->shouldReceive('post')
                         ->once()
                         ->with('tickets', [RequestOptions::JSON => $data])
                         ->andReturn($httpResponse);

        $response = $this->sut->create($data);

        $this->assertInstanceOf(\stdClass::class, $response);
        $this->assertEquals($response->json, 'response');
    }
}
