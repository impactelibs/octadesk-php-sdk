<?php

namespace Tests;

use GuzzleHttp\Psr7\Response;
use Mockery as m;

trait FakeHttpResponse
{
    protected function createFakeHttpResponse()
    {
        $httpResponse = m::mock(Response::class);

        $httpResponse->shouldReceive("getBody")
                     ->once()
                     ->andReturnSelf();

        $httpResponse->shouldReceive("getContents")
                     ->once()
                     ->andReturn('{"json":"response"}');

        return $httpResponse;
    }
}
