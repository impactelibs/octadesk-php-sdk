<?php

namespace Impacte\Octadesk;

use PHPUnit\Framework\TestCase;
use Impacte\Octadesk\Client;
use Impacte\Octadesk\Endpoints\Tickets;
use Impacte\Octadesk\Endpoints\Subjects;
use Mockery as m;

class ClientTest extends TestCase
{
    /** @test */
    public function fail_when_instantiating_with_invalid_credentials()
    {
        $this->expectException(\GuzzleHttp\Exception\ClientException::class);

        $sut = new Client('username', 'apitoken');
    }

    /** @test */
    public function should_return_endpoint_corresponding_to_method_called()
    {
        $client = m::mock(Client::class)
            ->makePartial()
            ->shouldAllowMockingProtectedMethods();

        $client->shouldReceive('fetchToken')->once()->andReturn("token");

        $ticketsEndpoint = $client->tickets();
        $subjectsEndpoint = $client->subjects();
        
        $this->assertInstanceOf(Tickets::class, $ticketsEndpoint); 
        $this->assertInstanceOf(Subjects::class, $subjectsEndpoint); 
    }
}
