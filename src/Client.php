<?php

namespace Impacte\Octadesk;

use GuzzleHttp\Client as HttpClient;
use Impacte\Octadesk\ResponseParser;

class Client implements ClientInterface
{
	protected $client;

	protected $currentToken;
    
    protected $apiToken;

    protected $username;

	public function __construct($username, $apiToken)
    {
        $this->username = $username;
        $this->apiToken = $apiToken;

        $this->client = new HttpClient(['base_uri' => 'https://api.octadesk.services/']);
        $this->client = new ResponseParser($this->client);
        $this->getApiToken();
	}

    public function __call($methodName, $args)
    {
        $className = "Impacte\\Octadesk\\Endpoints\\".ucfirst($methodName); 
        
        if (class_exists($className)) {
            return new $className($this->getAuthenticatedClient());
        }

        throw new \Error("Uncaught Error: Call to undefined method Client::$methodName()");
    }

	protected function getApiToken()
	{
		$this->currentToken = $this->fetchToken()->token;
	}

    protected function fetchToken()
    {
        $credentials = [
            'apiToken' => $this->apiToken,
            'username' => $this->username
        ];

		return $this->client->post('login/apiToken', ['headers' => $credentials]);	
    }
    
    protected function getAuthenticatedClient()
    {
        return new HttpClient([
            'base_uri' => 'https://api.octadesk.services/', 
            'headers' => ['Authorization' => "Bearer {$this->currentToken}"]
        ]);
    }
}
