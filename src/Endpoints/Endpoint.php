<?php

namespace Impacte\Octadesk\Endpoints;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Response;
use Impacte\Octadesk\ResponseParser;

abstract class Endpoint
{
    protected $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = new ResponseParser($httpClient);
    }
}
