<?php

namespace Impacte\Octadesk\Endpoints;

use Impacte\Octadesk\Endpoints\Endpoint;

class Subjects extends Endpoint
{
    public function all()
    {
	    return $this->httpClient->get('subjects');
    }
}
