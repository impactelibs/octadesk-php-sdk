<?php

namespace Impacte\Octadesk\Endpoints;

use Impacte\Octadesk\Endpoints\Endpoint;

class Tickets extends Endpoint
{
    public function create($data)
    {
        return $this->httpClient->post('tickets', [
			\GuzzleHttp\RequestOptions::JSON => $data
        ]);
    }
}

