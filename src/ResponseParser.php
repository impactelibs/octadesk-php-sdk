<?php

namespace Impacte\Octadesk;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Psr7\Response;

class ResponseParser
{
    protected $httpClient;

    protected $methods = [
        'get',
        'post',
        'put'
    ];

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function __call($methodName, $args)
    {
        if (in_array($methodName, $this->methods)) {
            $response = call_user_func_array([$this->httpClient, $methodName], $args);
            return $this->parseResponse($response);
        }
    }
    
    protected function parseResponse(Response $response)
    {
        return json_decode($response->getBody()->getContents());
    }
}
